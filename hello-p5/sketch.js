/* eslint no-undef:0 */
/* eslint no-unused-vars:0 */

const size = 1000
let x1
let y1
let x2
let y2
let coordinates = [x1, y1, x2, y2]

function setup () {
  createCanvas(size, size)
  background(250)
  strokeWeight(1)
  stroke(110, 110, 110, 24)
  frameRate(60)
  for (let c of coordinates) c = getRandom(max)
}

function draw () {
  // set last stroke end point as next start point
  x1 = x2
  y1 = y2
  // randomize next end point
  y2 = getRandom(size)
  x2 = getRandom(size)
  line(x1, y1, x2, y2)
}

function getRandom (max) {
  return int(random(0, max))
}
