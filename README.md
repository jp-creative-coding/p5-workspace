# p5

[p5.js](https://p5js.org/) is an open source library for creative coding in JavaScript, created by [Lauren Lee McCarthy](https://lauren-mccarthy.com/).

It is based on [processing](https://processing.org/) and now part of the [Processing Foundation](https://processingfoundation.org/).

There's an awesome online [editor](https://editor.p5js.org) to play around with the library, I also have some [sketches](https://editor.p5js.org/johannprell/sketches) there.