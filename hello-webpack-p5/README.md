# Hello WebPack p5

Super simple test with webpack and p5.
Install and run with:
```
npm install
npm start
```

Getting p5 from static file in this example. Added random-int package as example of using 3rd party npm packages with p5 API.