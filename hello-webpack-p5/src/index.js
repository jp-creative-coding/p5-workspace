import * as p5 from './p5.min.js'
import randomInt from 'random-int'

console.log('hello webpack and p5!')

// Create sketch
let sketch
let s = (sk) => {
    sk.setup = setup
    sk.draw = draw
    sketch = sk
}
// Initiate p5
const P5 = new p5(s)

// Data
let frameCounter = 0
const config = {
    hello: 'hello webpack p5',
    text: {
        size: {
            min: 28,
            max: 56
        },
        position: {
            minX: -400,
            minY: -40
        },
        fill: {
            r: 250, g: 250, b: 250, a: 60
        },
        jump: {
            interval: 2, // in frames
        }
    },
    background: {
        color: 40
    }
}

// Run general init - could be used for a bunch of stuff outside p5
init()
function init() {
    if(P5) console.log('p5 initiated', P5)
}

function setup() {
    console.log('Setting up sketch...')
    sketch.createCanvas(window.innerWidth, window.innerHeight)
    sketch.background(config.background.color)

    const color = config.text.fill
    sketch.fill(color.r, color.g, color.b, color.a)
}

function draw() {
    if (frameCounter++ % config.text.jump.interval !== 0) return

    const size = randomInt(20, 80)
    const x = randomInt(config.text.position.minX, window.innerWidth)
    const y = randomInt(config.text.position.minY, window.innerHeight)

    sketch.textSize(size)
    sketch.text(config.hello, x, y)
}

